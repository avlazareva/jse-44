package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;
import ru.t1.lazareva.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDto> extends IUserOwnedDtoRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

}
