package ru.t1.lazareva.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.exception.entity.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDto, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    public AbstractUserOwnedDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedDtoRepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null) return false;
        @Nullable Boolean resultModel = false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) return false;
        @Nullable Boolean resultModel = false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel =  repository.existsByIndex(userId, index);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable List<M> resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel =  repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @Nullable List<M> resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel = repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable List<M> resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel =  repository.findAll(userId, sort.getComparator());
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel =  repository.findOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable int resultModel = 0;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            resultModel = repository.getSize(userId);
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @Nullable M result = findOneById(userId, id);
        remove(userId, result);
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        @Nullable M result = findOneByIndex(userId, index);
        remove(userId, result);
    }

    @Override
    public M update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultModel = repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultModel;
    }

}