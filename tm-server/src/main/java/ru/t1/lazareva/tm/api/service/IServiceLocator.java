package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.service.dto.*;
import ru.t1.lazareva.tm.api.service.model.ISessionService;

public interface IServiceLocator {

    @NotNull
    IAuthService getAuthService();

    @NotNull
    @SuppressWarnings("unused")
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionDtoService getSessionService();

}