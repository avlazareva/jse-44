package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.enumerated.Status;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;
    void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String projectId) throws Exception;

}

