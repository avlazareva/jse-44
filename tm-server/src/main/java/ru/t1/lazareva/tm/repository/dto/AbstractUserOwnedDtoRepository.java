package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    public AbstractUserOwnedDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        return findOneByIndex(userId, index) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws Exception {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        @NotNull String orderStatement = " ORDER BY m." + getSortType(comparator);
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .getResultList();
}

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.index = :index";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("index", index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        entityManager.remove(model);
    }

    @Override
    public M update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        return entityManager.merge(model);
    }

}

