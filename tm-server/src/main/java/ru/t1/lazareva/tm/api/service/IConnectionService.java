package ru.t1.lazareva.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    SqlSession getSqlSession();

    EntityManagerFactory getEMFactory();

    EntityManager getEntityManager();

}
