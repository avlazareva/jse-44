package ru.t1.lazareva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.model.IProjectRepository;
import ru.t1.lazareva.tm.comparator.CreatedComparator;
import ru.t1.lazareva.tm.comparator.NameComparator;
import ru.t1.lazareva.tm.comparator.StatusComparator;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = getClazz().newInstance();
        project.setUser(entityManager.find(User.class, userId));
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Project project = getClazz().newInstance();
        project.setUser(entityManager.find(User.class, userId));
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    protected Class<Project> getClazz() {
        return Project.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

}
