package ru.t1.lazareva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.model.IWBS;
import ru.t1.lazareva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm.tm_project")
public final class ProjectDto extends AbstractUserOwnedModelDto implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @Nullable
    @Column(nullable = true, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    public ProjectDto(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDto(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectDto(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

    public ProjectDto(@NotNull final UserDto user) {
        setUserId(user.getId());
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}