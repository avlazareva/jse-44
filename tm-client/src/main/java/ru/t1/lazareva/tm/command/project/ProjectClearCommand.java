package ru.t1.lazareva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-clear";

    @NotNull
    private static final String DESCRIPTION = "Delete all projects.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProject(request);
    }

}